import {GET_REPOSITORIES_FAIL, GET_REPOSITORIES_START, GET_REPOSITORIES_SUCCESS} from "../actions";

const initialState = {
  loading: true,
  items: null
};

export default function (state = initialState, { type, payload }) {
  switch (type) {
    case GET_REPOSITORIES_START:
      return {
        ...state,
        loading: true,
      };
    case GET_REPOSITORIES_SUCCESS:
      return {
        ...state,
        loading: false,
        items: payload
      };
    case GET_REPOSITORIES_FAIL:
      return {
        ...state,
        loading: false
      };
    default:
      return state;
  }
}