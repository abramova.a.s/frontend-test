import {GET_REPOSITORIES_FAIL, GET_REPOSITORIES_START, GET_REPOSITORIES_SUCCESS} from "../actions";

export function getRepositoriesStart() {
  return { type: GET_REPOSITORIES_START }
}

export function getRepositoriesSuccess(repositories) {
  return {
    type: GET_REPOSITORIES_SUCCESS,
    payload: repositories
  }
}

export function getRepositoriesFail() {
  return { type: GET_REPOSITORIES_FAIL }
}