import {applyMiddleware, compose, createStore} from 'redux'
import {createWrapper, HYDRATE} from 'next-redux-wrapper'
import thunkMiddleware from 'redux-thunk'
import rootReducer from "./rootReducer";

const composeEnhancers =
  typeof window === 'object' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({})
    : compose;

const reducer = (state, action) => {
  if (action.type === HYDRATE) {
    return {
      ...state, // use previous state
      ...action.payload, // apply delta from hydration
    }
  } else {
    return rootReducer(state, action)
  }
};

const makeStore = initialState => createStore(reducer,  initialState, composeEnhancers(applyMiddleware(thunkMiddleware)));

export const wrapper = createWrapper(makeStore);