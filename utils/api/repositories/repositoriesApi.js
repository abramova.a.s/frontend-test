import {getRepositoriesFail, getRepositoriesStart, getRepositoriesSuccess} from "../../redux/repositories/actions"

function getRepositories(page) {
  return async dispatch => {
    dispatch(getRepositoriesStart());
    try {
      let link = 'https://api.github.com/repositories';
      let response = {};
      do {
        response = await fetch(link);
        link = response.headers.get('link').match(/\<(.*?)\>/)[1];
        page--;
      } while (page);
      const repositories = await response.json();
      dispatch(getRepositoriesSuccess(repositories));
    } catch (e) {
      dispatch(getRepositoriesFail(e));
    }
  }
}

export default {
  get: getRepositories
}