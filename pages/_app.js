import 'bootstrap/dist/css/bootstrap.min.css';
import { wrapper } from "../utils/redux/store";

const SmartForceApp = ({ Component, pageProps }) => {
  return <>
    <Component {...pageProps} />
    </>
};

export default wrapper.withRedux(SmartForceApp)
