import api from '../utils/api';
import {wrapper} from "../utils/redux/store";
import RepositoriesPage from "../components/pages/RepositoriesPage";

export default () => <RepositoriesPage/>

export const getStaticProps = wrapper.getStaticProps(async (
  {store}
  ) => {
  await store.dispatch(api.repositories.get(1))
});
