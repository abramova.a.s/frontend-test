import api from '../../utils/api'
import {wrapper} from "../../utils/redux/store";
import RepositoriesPage from "../../components/pages/RepositoriesPage";

export default ({page}) => <RepositoriesPage page={page}/>

export const getStaticProps = wrapper.getStaticProps(async ({ store, params}) => {
  await store.dispatch(api.repositories.get(params.page));
  return {
    props: params
  }
});

export async function getStaticPaths() {
  return {
    paths: [
      {params: { page: '1' }},
      {params: { page: '2' }},
      {params: { page: '3' }},
      {params: { page: '4' }},
      {params: { page: '5' }},
    ],
    fallback: false
  }
}
