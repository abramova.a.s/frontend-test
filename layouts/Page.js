import {Container, Navbar} from "react-bootstrap";
import styled from "styled-components";

const PageContainer = styled(Container)`
  padding-top: 100px;
`;

function Page({children}) {
  return (
    <div className='page'>
      <Navbar fixed='top' bg='dark' variant='dark'>
        <Navbar.Brand>Frontend Test</Navbar.Brand>
      </Navbar>
      <PageContainer>
        {children}
      </PageContainer>
    </div>
  )
}

export default Page;