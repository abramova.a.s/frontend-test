This project is developed as test task.

# Task

Create new React app to display list of Github repositories. Page should fetch repositories using Github API and show list on the page. App should use Redux, ES6 syntax and css in js (Styled components). If required use Express.js for backend.

## Extra Goals (done)

1. Pagination.
2. SSR.

### Available Scripts

In the project directory, you can run:

### `npm run dev`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

                