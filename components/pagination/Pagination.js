import BootstrapPagination from "react-bootstrap/cjs/Pagination";
import Link from "next/link";


function Pagination({currentPage}) {
  const pageLink = (number) => `/page/${number}`;
  return (
    <BootstrapPagination>
      {currentPage === 1
        ? <>
            <BootstrapPagination.First disabled />
            <BootstrapPagination.Prev disabled />
          </>
        : (
          <>
          <Link href={pageLink(1)}>
            <li className="page-item">
              <a className="page-link">
                <span aria-hidden="true">«</span>
                <span className="sr-only">First</span>
              </a>
            </li>
          </Link>
          <Link href={pageLink(currentPage - 1)}>
            <li className="page-item">
              <a className="page-link" href="/page/3">
                <span aria-hidden="true">‹</span>
                <span className="sr-only">Previous</span>
              </a>
            </li>
          </Link>
          </>
        )
      }

      <BootstrapPagination.Item active>
        {currentPage}
      </BootstrapPagination.Item>

      {currentPage === 5
        ? <>
            <BootstrapPagination.Next disabled />
            <BootstrapPagination.Last disabled />
          </>

        : (
          <>
            <Link href={pageLink(currentPage + 1)}>
              <li className="page-item">
                <a className="page-link" role="button">
                  <span aria-hidden="true">›</span><span className="sr-only">Next</span>
                </a>
              </li>
            </Link>
            <Link href={pageLink(5)} forwardRef>
              <li className="page-item">
                <a className="page-link">
                  <span aria-hidden="true">»</span><span className="sr-only">Last</span>
                </a>
              </li>
            </Link>
          </>
        )
      }



    </BootstrapPagination>
  )
}

export default Pagination;