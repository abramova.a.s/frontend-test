import {Card, Col, Image, Row} from "react-bootstrap";
import styled from "styled-components";

function Repositories({items}) {

  const OwnerImage = styled(Image)`
    width: 30px;
    border-radius: 50%;
  `;

  return (
    <Row>
      {items.map(item => (
        <Col xs={4}>
          <Card key={item.id} className='mb-2'>
            <Card.Header className='justify-content-between d-flex'>
              {item.owner.login}
              <OwnerImage src={item.owner.avatar_url}/>
            </Card.Header>
            <Card.Body>
              <Card.Title><a href={item.html_url} target='_blank'>{item.name}</a></Card.Title>
              {item.description}
            </Card.Body>
          </Card>
        </Col>
      ))}
    </Row>
  )
}

export default Repositories;