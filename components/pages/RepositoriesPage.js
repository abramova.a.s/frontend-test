import Repositories from "../repositories/Repositories";
import Pagination from "../pagination/Pagination";
import Page from "../../layouts/Page";
import {useSelector} from "react-redux";

function RepositoriesPage({page}) {
  const repositories = useSelector(state => state.repositories.items);

  return <Page>

    <h1 className='mb-4'>GitHub Repositories</h1>
      <Repositories items={repositories}/>
      <div className='d-flex justify-content-center'>
        <Pagination currentPage={Number(page)} />
      </div>
  </Page>
}

export default RepositoriesPage;